<?php

/**
 * @file
 * Install and update hooks for BitPay module.
 */

/**
 * Implements hook_schema().
 */
function bitpay_schema() {
  $schema['bitpay'] = [
    'description' => 'Stores bitpay invoices.',
    'fields' => [
      'invoice_id' => [
        'description' => 'Primary key: invoice identifier.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => TRUE,
      ],
      'order_id' => [
        'description' => 'Order identifier.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ],
      'amount_paid' => [
        'description' => 'The amount paid in smallest possible transaction currency unit.',
        'type' => 'numeric',
        'size' => 'normal',
        'precision' => 24,
        'scale' => 6,
        'not null' => FALSE,
      ],
      'currency' => [
        'description' => 'Currency code.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ],
      'price' => [
        'description' => 'The invoice price.',
        'type' => 'numeric',
        'size' => 'normal',
        'precision' => 16,
        'scale' => 6,
        'not null' => FALSE,
      ],
      'status' => [
        'description' => 'Last reported invoice status.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
      ],
      'exception_status' => [
        'description' => 'May be paidOver if invoice was overpaid or paidPartial if invoice was underpaid.',
        'type' => 'varchar',
        'length' => 64,
        'not null' => FALSE,
      ],
      'created' => [
        'description' => 'The Unix timestamp when this record was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'changed' => [
        'description' => 'The Unix timestamp when this record was most recently changed.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'pos_data' => [
        'description' => 'Any passthrough data provided with the original invoice.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => FALSE,
      ],
      'buyer_email' => [
        'description' => 'The buyer email address.',
        'type' => 'varchar',
        'length' => 128,
        'not null' => FALSE,
      ],
      'transaction_currency' => [
        'description' => 'Transaction currency code.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => FALSE,
      ],
    ],
    'primary key' => ['invoice_id'],
    'indexes' => [
      'order_id' => ['order_id'],
      'changed' => ['changed'],
      'created' => ['created'],
      'status' => ['status'],
      'exception_status' => ['exception_status'],
    ],
  ];
  return $schema;
}

/**
 * Add transaction_currency column.
 */
function bitpay_update_8101() {
  db_add_field('bitpay', 'transaction_currency', [
    'description' => 'Transaction currency code.',
    'type' => 'varchar',
    'length' => 32,
    'not null' => FALSE,
  ]);
}

/**
 * Rename btc_paid to amount_paid.
 */
function bitpay_update_8102() {
  db_change_field('bitpay', 'btc_paid', 'amount_paid', [
    'description' => 'The amount paid in smallest possible transaction currency unit.',
    'type' => 'numeric',
    'size' => 'normal',
    'precision' => 24,
    'scale' => 6,
    'not null' => FALSE,
  ]);
}

/**
 * Change amount_paid units to satoshis.
 */
function bitpay_update_8103() {
  db_update('bitpay')->expression('amount_paid', 'amount_paid * 100000000')->execute();
}

/**
 * Remove obsolete btc_due, btc_price and rate fields.
 */
function bitpay_update_8104() {
  db_drop_field('bitpay', 'btc_due');
  db_drop_field('bitpay', 'btc_price');
  db_drop_field('bitpay', 'rate');
}
