<?php

namespace Drupal\bitpay\Controller;

use Bitpay\Bitpay;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Receives BitPay webhooks and invokes hook_bitpay_invoice_update().
 */
class BitpayController extends ControllerBase {

  /**
   * {@inheritdoc}
   *
   * @param \Bitpay\Bitpay $bitpay
   *   The Bitpay service.
   */
  public function __construct(Bitpay $bitpay) {
    $this->bitpay = $bitpay;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static (
      $container->get('bitpay.service')
    );
  }

  /**
   * Callback for processing webhooks.
   */
  public function webhookCallback(Request $request) {
    $webhook = json_decode($request->getContent());
    if (empty($webhook->id)) {
      throw new BadRequestHttpException('Invalid webhook.');
    }
    $client = $this->bitpay->getClient();
    $invoice = $client->getInvoice($webhook->id);
    $this->moduleHandler()->invokeAll('bitpay_invoice_update', [$invoice]);
    return new Response(NULL, 204);
  }

}
