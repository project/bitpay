<?php

namespace Drupal\bitpay;

use Bitpay\Client\Client;
use Bitpay\InvoiceInterface;
use Drupal\Core\Url;

/**
 * Client used to send requests and receive responses for BitPay's Web API.
 */
class BitpayClient extends Client {

  /**
   * {@inheritdoc}
   */
  public function createInvoice(InvoiceInterface $invoice) {
    $invoice->setNotificationUrl(Url::fromRoute('bitpay.callback', [], ['absolute' => TRUE, 'https' => TRUE])->toString());
    parent::createInvoice($invoice);
    $this->writeRecord($invoice);
    return $invoice;
  }

  /**
   * {@inheritdoc}
   */
  public function getInvoice($invoiceId) {
    $invoice = parent::getInvoice($invoiceId);
    $this->writeRecord($invoice);
    return $invoice;
  }

  /**
   * Records an invoice to the local database.
   */
  public function writeRecord($invoice) {
    $fields['changed'] = REQUEST_TIME;
    $fields['order_id'] = $invoice->getOrderId();
    $fields['created'] = $invoice->getInvoiceTime()->getTimestamp();
    $fields['amount_paid'] = $invoice->getAmountPaid();
    $fields['currency'] = $invoice->getCurrency()->getCode();
    $fields['exception_status'] = $invoice->getExceptionStatus();
    $fields['price'] = $invoice->getPrice();
    $fields['status'] = $invoice->getStatus();
    $fields['pos_data'] = $invoice->getPosData();
    $fields['transaction_currency'] = $invoice->getTransactionCurrency();
    if ($invoice->getBuyerEmail()) {
      $fields['buyer_email'] = $invoice->getBuyerEmail();
    }
    \Drupal::database()->merge('bitpay')
      ->key(['invoice_id' => $invoice->getId()])
      ->fields($fields)
      ->execute();
  }

}
