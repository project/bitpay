<?php

namespace Drupal\bitpay\Form;

use Bitpay\Bitpay;
use Bitpay\PrivateKey;
use Bitpay\PublicKey;
use Bitpay\SinKey;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides BitPay settings form.
 */
class BitpaySettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bitpay_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('bitpay.settings');
    $networks = ['livenet' => 'pairing_code_livenet', 'testnet' => 'pairing_code_testnet'];
    foreach ($networks as $network => $setting) {
      if ($pairing_code = $form_state->getValue($setting)) {
        $this->createToken($config, $network, $pairing_code);
      }
    }
    $config->set('network', $form_state->getValue('network'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Creates and saves a key pair and token.
   */
  public function createToken($config, $network, $pairing_code) {
    global $base_url;
    $config->set("token_$network", NULL);
    $config->set("key_storage_password_$network", NULL);
    $password = Crypt::randomBytesBase64();
    $bitpay = new Bitpay([
      'bitpay' => [
        'key_storage_password' => $password,
        'network' => $network,
        'private_key' => "private://bitpay_$network.key",
        'public_key' => "private://bitpay_$network.pub",
      ],
    ]);
    try {
      // Generate and store private key.
      $keyManager = $bitpay->get('key_manager');
      $privateKey = new PrivateKey($bitpay->getContainer()->getParameter('bitpay.private_key'));
      $privateKey->generate();
      $keyManager->persist($privateKey);
      // Generate and store public key.
      $publicKey = new PublicKey($bitpay->getContainer()->getParameter('bitpay.public_key'));
      $publicKey->setPrivateKey($privateKey);
      $publicKey->generate();
      $keyManager->persist($publicKey);
    }
    catch (\Exception $e) {
      drupal_set_message($this->t('Failed to create key pair: %message', ['%message' => $e->getMessage()]), 'error');
      return;
    }
    // Create API access token.
    $sin = new SinKey();
    $sin->setPublicKey($publicKey);
    $sin->generate();
    $client = $bitpay->get('client');
    try {
      $token = $client->createToken([
        'id' => (string) $sin,
        'pairingCode' => $pairing_code,
        'label' => $base_url,
      ]);
    }
    catch (\Exception $e) {
      drupal_set_message($this->t('Failed to create @network token: %message', [
        '%message' => $e->getMessage(),
        '@network' => $network,
      ]), 'error');
      return;
    }
    $config->set("token_$network", (string) $token);
    $config->set("key_storage_password_$network", $password);
    drupal_set_message($this->t('New @network API token generated successfully. Encrypted keypair saved to the private filesystem.', ['@network' => $network]));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['bitpay.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bitpay.settings');
    $form['token_livenet'] = [
      '#type' => 'item',
      '#title' => $this->t('Live API token'),
      '#description' => $config->get('token_livenet') ? $this->t('Configured') : $this->t('Not configured'),
    ];
    $form['pairing_code_livenet'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Live pairing code'),
      '#description' => $this->t('Visit your <a href="https://bitpay.com/dashboard/merchant/api-tokens">Manage API Tokens page</a>, click the "Add New Token" button, leave the "Require Authentication" checkbox checked, and enter the pairing code here.'),
      '#default_value' => $config->get('pairing_code_livenet'),
    ];
    $form['token_testnet'] = [
      '#type' => 'item',
      '#title' => $this->t('Test API token'),
      '#description' => $config->get('token_testnet') ? $this->t('Configured') : $this->t('Not configured'),
    ];
    $form['pairing_code_testnet'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test pairing code'),
      '#description' => $this->t('Visit your <a href="https://test.bitpay.com/dashboard/merchant/api-tokens">Manage API Tokens page</a>, click the "Add New Token" button, leave the "Require Authentication" checkbox checked, and enter the pairing code here.'),
      '#default_value' => $config->get('pairing_code_testnet'),
    ];
    $form['network'] = [
      '#type' => 'radios',
      '#title' => $this->t('Environment'),
      '#description' => $this->t('Test mode uses the <a href="https://test.bitpay.com/">Bitpay test environment</a> and allows payment with free Testnet coins.'),
      '#options' => [
        'livenet' => $this->t('Live'),
        'testnet' => $this->t('Test'),
      ],
      '#default_value' => $config->get('network'),
    ];
    return parent::buildForm($form, $form_state);
  }

}
