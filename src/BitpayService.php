<?php

namespace Drupal\bitpay;

use Bitpay\Bitpay;
use Bitpay\Token;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Bitpay service.
 */
class BitpayService extends Bitpay {

  /**
   * {@inheritdoc}
   */
  public function __construct($config = [], ContainerInterface $container = NULL, ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    if (!isset($config['bitpay'])) {
      $config['bitpay'] = [];
    }
    $settings = $this->configFactory->get('bitpay.settings');
    $config['bitpay'] += ['network' => $settings->get('network')];
    $network = $config['bitpay']['network'];
    $config['bitpay'] += [
      'key_storage_password' => $settings->get("key_storage_password_$network"),
      'private_key' => "private://bitpay_$network.key",
      'public_key' => "private://bitpay_$network.pub",
    ];
    parent::__construct($config, $container);
  }

  /**
   * Loads BitPay settings and returns Bitpay client object.
   *
   * @return \Bitpay\Client\ClientInterface
   *   BitPay client.
   *
   * @throws \Exception
   *   If client cannot be initialized.
   */
  public function getClient() {
    $definition = $this->getContainer()->findDefinition('client');
    $definition->setClass('Drupal\bitpay\BitpayClient');
    $client = $this->get('client');
    $client->setPrivateKey($this->get('key_manager')->load($this->getContainer()->getParameter('bitpay.private_key')));
    $client->setPublicKey($this->get('key_manager')->load($this->getContainer()->getParameter('bitpay.public_key')));
    $token = new Token();
    $network = $this->get('network')->getName();
    $token->setToken($this->configFactory->get('bitpay.settings')->get("token_$network"));
    $client->setToken($token);
    return $client;
  }

  /**
   * Returns a renderable array for the BitPay invoice iframe.
   */
  public function getInvoiceIframe($invoice_id) {
    $element['#type'] = 'html_tag';
    $element['#tag'] = 'iframe';
    $element['#attributes']['class'] = 'bitpay-invoice';
    $element['#attributes']['src'] = $this->getInvoiceUrl($invoice_id, 'iframe');
    $element['#value'] = '';
    $element['#attached']['library'][] = 'bitpay/bitpay.iframe';
    return $element;
  }

  /**
   * Generates BitPay invoice URL.
   */
  public function getInvoiceUrl($invoice_id, $view = '') {
    return Url::fromUri("https://{$this->get('network')->getApiHost()}/invoice", ['query' => ['id' => $invoice_id, 'view' => $view]])->toString();
  }

}
