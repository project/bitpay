CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * API
 * Requirements
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

BitPay module provides tools for integrating BitPay, a cryptocurrency payment
processor.

This module doesn't do anything on its own, it's a library for developers. You
will need to write code to generate an invoice, display a payment iframe, and
act on invoice status updates. However, this module does make all of that
easier.

See https://supporters.eff.org/donate for an example of a site using this
module on the backend to receive donations and membership dues via Bitcoin.

This module is not affiliated with BitPay, Inc. It has been developed
independently.

REQUIREMENTS
------------

If you have not already, configure your private file system path in
settings.php and clear all caches. A key pair for authenticating with the
BitPay API will be generated and stored in the private file system.

Ensure that bitpay/php-client has been installed by composer. This should
happen automatically if you are using composer to build your site.

CONFIGURATION
-------------

Visit admin/config/services/bitpay to configure BitPay API access.

You will need to generate a new API token at
https://bitpay.com/dashboard/merchant/api-tokens (livenet) or
https://test.bitpay.com/dashboard/merchant/api-tokens (testnet) and paste the
pairing code into the configuration form.

DEVELOPER API
-------------

API methods will throw an exception in the event of any error.

To create an invoice:

  $invoice = new \Bitpay\Invoice;
  $currency = new \Bitpay\Currency();
  $currency->setCode('USD');
  $invoice->setCurrency($currency);
  $invoice->setPrice(19.95);
  $bitpay = \Drupal::getContainer()->get('bitpay.service');
  $client = $bitpay->getClient();
  $client->createInvoice($invoice);

To display a payment iframe:

  $element = [
    '#theme' => 'bitpay_invoice_iframe',
    '#invoice_id' => $invoice->getId(),
  ];

To respond to invoice status updates, implement hook_bitpay_invoice_update().

To get an invoice:

  $bitpay = \Drupal::getContainer()->get('bitpay.service');
  $client = $bitpay->getClient();
  $invoice = $client->getInvoice($invoiceId);

TROUBLESHOOTING
---------------

Check your logs; any exceptions should be logged by Drupal.

MAINTAINERS
-----------

Mark Burdett (mfb) - https://www.drupal.org/u/mfb
	
This project has been sponsored by:
 * EFF https://www.eff.org/
