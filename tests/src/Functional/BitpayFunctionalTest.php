<?php

namespace Drupal\Tests\bitpay\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests basic BitPay functionality.
 *
 * @group bitpay
 */
class BitpayFunctionalTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['bitpay'];

  /**
   * Tests unconfigured BitPay client.
   *
   * @expectedException Exception
   * @expectedExceptionMessage Could not find "private://bitpay_livenet.key"
   */
  public function testClient() {
    $this->container->get('bitpay.service')->getClient();
  }

  /**
   * Tests BitPay configuration.
   */
  public function testConfiguration() {
    $user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($user);
    $this->drupalPostForm('admin/config/services/bitpay', ['network' => 'testnet'], t('Save configuration'));
    $bitpay = $this->container->get('bitpay.service');
    $this->assertInstanceOf('Bitpay\Bitpay', $bitpay);
    $url = $bitpay->getInvoiceUrl('EXAMPLE');
    $this->assertSame($url, 'https://test.bitpay.com/invoice?id=EXAMPLE&view=');
  }

}
